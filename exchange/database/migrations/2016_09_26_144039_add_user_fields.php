<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('login')->after('id')->unique()->comment = '';
            $table->string('surname')->comment = 'Фамилия';
            $table->integer('city_id')->nullable()->comment = 'Город';
            $table->integer('phone')->nullable()->comment = 'Phone';
            $table->boolean('sending')->comment = 'agreement with the sending';
            $table->boolean('agreement')->comment = 'consent to the processing of personal data';
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
