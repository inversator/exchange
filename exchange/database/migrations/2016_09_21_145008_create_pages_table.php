<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table){
            $table->increments('id', 4)->comment = '������������� ��������';
            $table->string('meta_k', 255)->comment = '�������� �����';
            $table->string('meta_d', 255)->comment = '�������� �������� ��� �����������';
            $table->string('bTitle', 255)->comment = '��������� � ��������';
            $table->string('pTitle', 255)->comment = '��������� ��������';
            $table->text('desc')->comment = '������� ��������';
            $table->text('text')->comment = '�����';
            $table->integer('off')->default(0)->comment = '1 ���� ���������';
            $table->string('url', 255)->comment = 'URL ����� ��������';
            $table->timestamps('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
