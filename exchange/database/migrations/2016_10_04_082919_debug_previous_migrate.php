<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DebugPreviousMigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function(Blueprint $table){
            $table->renameColumn('typeShort','type_short');
        });
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('city');
            $table->bigInteger('city_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
