<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_d')">
    <meta name="keywords" content="@yield('meta_k')">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>@yield('bTitle')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/bootstrap.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/main.css') }}"/>
    @yield('topStyle')
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="container">
    @include('blocks.header')
    @include('blocks.navbar')
</div>
<div class="container">
    <div class="row mainButtons">
        <div class="col-xs-12 col-sm-6">
            <a class="btn btn-primary" href="/ads/sale">
                <span class="glyphicon glyphicon-thumbs-up"></span>
                КУПИТЬ МЕТАЛЛ
            </a>
        </div>
        <div class="col-xs-12 col-sm-6">
            <a class="btn btn-primary" href="/ads/purchase">
                <span class="glyphicon glyphicon-usd"></span>
                ПРОДАТЬ МЕТАЛЛ
            </a>
        </div>


        <div class="tab-content">
            <div class="tab-pane" id="buy">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">Объявления</a></li>
                    <li><a href="#profile" data-toggle="tab">Аукционы</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home">

                    </div>
                    <div class="tab-pane" id="profile">
                        <table class="table table-striped">
                            <tr>
                                <th>Наименование</th>
                                <th>Тип</th>
                                <th>Вес (тонны)</th>
                                <th>Регион</th>
                                <th>Срок аукциона</th>
                                <th>Продавец</th>
                                <th>Ставка</th>
                                <th>Выкупить</th>
                            </tr>
                            <tr>
                                <td>Свинец</td>
                                <td>S2S</td>
                                <td>20</td>
                                <td>Москва</td>
                                <td>10 дней</td>
                                <td>Артем</td>
                                <td>10 000 $</td>
                                <td>20 000 $</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="sell">
                <div class="ads">
                    <div class="ad col-sm-4">
                        <div class="block1">
                            <div class="name col-sm-6 label label-success">РязЦветМет</div>
                            <div class="date col-sm-6 label label-primary">12 октября 2015</div>
                        </div>
                        <div class="block2">
                            <div class="text">
                                <table class="table">
                                    <tr>
                                        <td>Тип лома:</td>
                                        <td>СВИНЕЦ</td>
                                    </tr>
                                    <tr>
                                        <td>Объем(кг):</td>
                                        <td>12 000</td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td>8 (800) 888-99-00</td>
                                    </tr>
                                    <tr>
                                        <td>Тип сделки:</td>
                                        <td>За наличные</td>
                                    </tr>
                                    <tr>
                                        <td>Цена (руб):</td>
                                        <td>По договренности</td>
                                    </tr>
                                    <tr>
                                        <td>Город</td>
                                        <td>Рязань</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="block3">
                            <div class="reiting">
                                <span class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span>
                            </div>
                        </div>
                    </div>
                    <div class="ad col-sm-4">
                        <div class="block1">
                            <div class="name col-sm-6 label label-success">ИП "ПЕТРОВ"</div>
                            <div class="date col-sm-6 label label-primary">11 октября 2015</div>
                        </div>
                        <div class="block2">
                            <div class="text">
                                <table class="table">
                                    <tr>
                                        <td>Тип лома:</td>
                                        <td>СТАЛЬ</td>
                                    </tr>
                                    <tr>
                                        <td>Объем(кг):</td>
                                        <td>50 000</td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td>8 (800) 888-99-00</td>
                                    </tr>
                                    <tr>
                                        <td>Тип сделки:</td>
                                        <td>За наличные</td>
                                    </tr>
                                    <tr>
                                        <td>Цена (руб):</td>
                                        <td>300 000</td>
                                    </tr>
                                    <tr>
                                        <td>Город</td>
                                        <td>Москва</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="block3">
                            <div class="reiting">
                                <span class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span><span
                                        class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </div>
                        </div>
                    </div>
                    <div class="ad col-sm-4">
                        <div class="block1">
                            <div class="name col-sm-6 label label-success">Мрыхин Евгений</div>
                            <div class="date col-sm-6 label label-primary">10 сентября 2015</div>
                        </div>
                        <div class="block2">
                            <div class="text">
                                <table class="table">
                                    <tr>
                                        <td>Тип лома:</td>
                                        <td>АККУМУЛЯТОРЫ</td>
                                    </tr>
                                    <tr>
                                        <td>Объем(кг):</td>
                                        <td>1</td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td>8 (800) 888-99-00</td>
                                    </tr>
                                    <tr>
                                        <td>Тип сделки:</td>
                                        <td>За наличные</td>
                                    </tr>
                                    <tr>
                                        <td>Цена (руб):</td>
                                        <td>3000</td>
                                    </tr>
                                    <tr>
                                        <td>Город</td>
                                        <td>Рязань</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="block3">
                            <div class="reiting">
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <hr>
</div>

{{--<div id="carousel-example-generic" class="carousel slide hidden-sm hidden-md hidden-lg" data-ride="carousel">--}}
    {{--<!-- Indicators -->--}}
    {{--<ol class="carousel-indicators">--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
        {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
    {{--</ol>--}}

    {{--<!-- Wrapper for slides -->--}}
    {{--<div class="carousel-inner">--}}
        {{--<div class="item active">--}}
            {{--<img src="/img/slide1.png" alt=""/>--}}
        {{--</div>--}}
        {{--<div class="item">--}}
            {{--<img src="/img/slide2.png" alt=""/>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Controls -->--}}
    {{--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">--}}
        {{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    {{--</a>--}}
    {{--<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">--}}
        {{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    {{--</a>--}}
{{--</div>--}}
<div class="container">

    @yield('content')

    <hr>

    <footer>
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-12 logo"><p>&copy; Все права защищены. Лом.ру 2016 год.</p></div>

            <div class="col-md-8 col-sm-8 col-xs-12 topInfo">
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-4"><i class="glyphicon glyphicon-phone"></i> +7 (920) 987-00-23</div>
                    <div class="col-sm-5"><a style="text-align: right" href="mailto:admin@lom.ru" target="_blank">admin@lom.ru</a></div>
                </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12 callMe">
                <div class="btn btn-primary" onclick="location.href = '/ad/add'">
                   <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Подать объявление</a>
                </div>
            </div>
        </div>
    </footer>

</div>
<!--/.container-->

<!-- Modal CallMe -->
<div class="modal fade" id="callModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
            </div>
            <form id="callMe">
                <div class="modal-body">
                    <div class="form-group">
                        <input class="form-control" type="text" value="" placeholder="Иванов Иван Иванович"
                               name="edtName">
                    </div>

                    <div class="form-group">
                        <input type="text" value="" placeholder="(999) 999-99-99" name="edtPhone" class="form-control">
                    </div>


                    <div class="form-group">
                    <textarea name="txtComment" rows="5" cols="30" class="form-control"
                              placeholder="Ваше сообщение"></textarea>
                    </div>

                    <p>Наши менеджеры свяжутся с Вами в ближайшее время</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary">Отправить</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal ContactUs -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Заказать звонок</h4>
            </div>
            {{--<form id="callMe">--}}
                {{--<div class="modal-body">--}}
                    {{--<p>Оставьте сообщение, и наш специалист свяжется с Вами.</p>--}}

                    {{--<div class="form-group">--}}
                        {{--<input class="form-control" type="text" value="" placeholder="Иванов Иван Иванович"--}}
                               {{--name="edtName">--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<input class="form-control" type="text" value="" placeholder="Название организации"--}}
                               {{--name="edtName">--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<input type="text" value="" placeholder="(999) 999-99-99" name="edtPhone" class="form-control">--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                        {{--<input class="form-control" type="text" value="" placeholder="Ваш email" name="edtName">--}}
                    {{--</div>--}}

                    {{--<div class="form-group">--}}
                    {{--<textarea name="txtComment" rows="5" cols="30" class="form-control"--}}
                              {{--placeholder="Ваше сообщение"></textarea>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="modal-footer">--}}
                    {{--<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>--}}
                    {{--<button type="button" class="btn btn-primary">Отправить</button>--}}
                {{--</div>--}}
            {{--</form>--}}
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

<script src="/js/bootstrap.js"></script>
<script>$(document).ready(function () {
        $('[data-toggle=offcanvas]').click(function () {
            $('.row-offcanvas').toggleClass('active')
        });
    });
</script>
@yield('bottomScripts');
</body>
</html>