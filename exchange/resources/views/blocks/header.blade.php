    <header class="row">
        <div class="col-md-2 col-sm-2 col-xs-12 logo"><a href="/"><img src="/img/logo.png" alt=""/></a></div>

        <div class="col-md-8 col-sm-8 col-xs-12 topInfo">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-4"><i class="glyphicon glyphicon-phone"></i> +7 (920) 987-00-23</div>
                <div class="col-sm-5"><a style="text-align: right" href="mailto:admin@lom.ru" target="_blank">admin@lom.ru</a></div>
            </div>
        </div>

        <div class="col-md-2 col-sm-2 col-xs-12 callMe">
            <div class="btn btn-primary" onclick="location.href = '/ad/add'">
                <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;Подать объявление</a>
            </div>
        </div>
    </header>
