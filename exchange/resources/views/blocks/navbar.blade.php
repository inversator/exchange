<div role="navigation" class="navbar navbar-iveco">
    <div class="container-fluid">

        <div class="navbar-header">
            <div class="navbar-toggle">
                <div type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    <span>МЕНЮ</span>
                </div>
            </div>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/gosts">Типы металлов</a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">Объявления<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="/ads/sale">Купить металл</a></li>
                        <li><a href="/ads/purchase">Продать металл</a></li>
                    </ul>
                </li>
                @foreach($items as $item)
                    <li><a href="/{{$item->url}}">{{$item->name}}</a></li>
                @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">{{Auth::user()->login}}<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/profile">Профиль</a></li>
                            <li><a href="/ad/new">Добавить объявление</a></li>
                            <li><a href="/ad/list">Список объявлений</a></li>
                            <li><a href="/logout">Выйти</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/login">Войти</a></li>
                    <li><a href="/register">Регистрация</a></li>
                @endif
            </ul>
            <!-- my old menu -->
            <!--ul class="nav navbar-nav">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">МЕТАЛЛЫ<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Медь</a></li>
                        <li><a href="#">Латунь</a></li>
                        <li><a href="#">Свинец</a></li>
                        <li><a href="#">Аллюминий</a></li>
                        <li class="devider" role="separator"></li>
                        <li><a href="#">Чермет</a></li>
                    </ul>
                </li>
                <li><a href="/">АУКЦИОНЫ</a></li>

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropsown-toggle" href="#">УСЛУГИ<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Вывоз металла с объектов</a></li>
                        <li><a href="#">Демонтаж</a></li>
                        <li><a href="#">Переработка металла</a></li>
                        <li><a href="#">Продажа лома</a></li>
                        <li><a href="#">Покупка лома</a></li>
                        <li><a href="#">Консультации</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropsown-toggle" href="#">РАЗМЕСТИТЬ ОБЪЯВЛЕНИЕ<b
                                class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Регистрация</a></li>
                        <li><a href="#">Вход</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropsown-toggle" href="#">О КОМПАНИИ<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Новости</a></li>
                        <li><a href="#">О площадке</a></li>
                        <li><a href="/">КОНТАКТЫ</a></li>
                    </ul>
                </li>
                <li><a href="/">ПУНКТЫ ПРИЕМА</a></li>
                <li><a href="/">ТРАНСПОРТНЫЕ КОМПАНИИ</a></li>

            </ul-->
        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</div>
