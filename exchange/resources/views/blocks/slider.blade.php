<div id="carousel-example-generic2" class="carousel slide hidden-xs" data-ride="carouse2">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @foreach($items as $key => $item)
            <li data-target="#carousel-example-generic"
                data-slide-to="{{$key}}"
                @if(!$key) class="active"
                    @endif>

            </li>
            @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @foreach($items as $key => $item)
            <div class="item @if(!$key) active
            @endif">
                <img src="/uploads/slides/large/{{$item->image}}" alt=""/>
            </div>
        @endforeach
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic2" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic2" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
