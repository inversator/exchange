@extends('layouts.main')
@section('meta_k', 'Список объявлений')
@section('meta_d', 'Список объявлений')
@section('bTitle', 'Список объявлений')
@section('topStyle')

@endsection
@section('bottomScripts')

@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Список объявлений
                        <div class="pull-right"><a href="/ad/add"><i class="glyphicon glyphicon-plus"></i> Добавить объявление</a></div>
                    </div>
                    <div class="panel-body">

                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Металл</th>
                                <th>Вес</th>
                                <th>Стоимость</th>
                                <th>Тип</th>
                                <th></th>
                            </tr>
                            @forelse($ads as $ad)
                                <tr>
                                    <td>{{ $ad->id }}</td>
                                    <td>{{ $ad->getMetal()->title }}</td>
                                    <td>{{ $ad->volume }} кг</td>
                                    <td>{{ $ad->price }} руб</td>
                                    <td>
                                        @if($ad->adType == 1)
                                            Продаете
                                        @else
                                            Покупаете
                                        @endif
                                    </td>
                                    <td><a href="/ad/delete/{{ $ad->id }}">Удалить</a></td>
                                </tr>
                            @empty
                                <tr><td colspan="5"><div class="alert">Вы пока ничего не разместили</div></td></tr>
                            @endforelse
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3 id="grid-intro">Правила размещения объявления</h3>
                <ul>
                    <li> Разрешается иметь только одну учетную запись;</li>
                    <li>Пользователи биржи Лом.ру самостоятельно размещают объявления и несут ответственность за то, что размещенная информация соответствую правилам биржи и законодательству РФ.</li>
                    <li>Не размещайте одно и тоже объявление несколько раз;</li>
                    <li>Нельзя размещать запрещенную номенклатуру товаров;</li>
                    <li>Запрещается использование ненормативной лексики;</li>
                    <li>Размещайте объявления только о конкретном товаре;</li>
                    <li>Запрещено размещение объявлени рекламного характера;</li>
                    <li>Запрещается использовать программы автозагрузки для размещения объявления без соглашения с Лом.ру.</li>
                    <li>Недопустимо наличие в объявлении или учетной записи пользователя эротического, порнографического, экстремистского или иного содержания;</li>
                    <li>Имя в объявлении должно соответствовать действительности;</li>
                    <li>Поля раздела Данные партии обязательны к заполнению;</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
