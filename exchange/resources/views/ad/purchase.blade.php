@extends('layouts.main')
@section('meta_k', 'Продать металла')
@section('meta_d', 'Продать металла')
@section('bTitle', 'Продать металла')
@section('topStyle')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/kladr.css') }}"/>
@endsection
@section('bottomScripts')
    <script type="text/javascript" src="/js/maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $.mask.definitions['~'] = '[+-]';
            $('#sale #phone').mask('(999) 999-9999');
            $('#purchase #phone').mask('(999) 999-9999');
        });
    </script>
    <script type="text/javascript" src="/js/kladr.js"></script>
    <script>
        $(function () {
            $('#sale [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#individual #city_id').val(obj.id);
                        $('#individual #type').val(obj.type);
                        $('#individual #type_short').val(obj.typeShort);
                        $('#individual #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        $(function () {
            $('#purchase [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#entity #city_id').val(obj.id);
                        $('#entity #type').val(obj.type);
                        $('#entity #type_short').val(obj.typeShort);
                        $('#entity #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        //        })
        $(document).ready(function () {

            var storage = localStorage.getItem('item');

            if (storage && storage !== "#") {
                $('.nav-tabs a[href="' + storage + '"]').tab('show');
            }

            var list = $('ul.nav').find('li');

            $('ul.nav').on('click', 'li:not(.active)', function () {
                var itemId = $(this).find('a').attr('href');
                localStorage.setItem('item', itemId);
            });
        });
    </script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('text-sale', {
            customConfig: 'config.js',
            toolbar: [
                ['Source', '-', 'Bold', 'Italic']
            ]
        });
        CKEDITOR.replace('text-purchase', {
            customConfig: 'config.js',
            toolbar: [
                ['Source', '-', 'Bold', 'Italic']
            ]
        });
    </script>
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h1>Продать металл</h1>
        </div>
        <div class="col-xs-12 col-sm-6 text-right">{{$ads->render()}} </div>
    </div>

    <div class="ads row">
        @forelse($ads as $ad)
            <div class="ad col-sm-4">
                <div class="block1">
                    <div class="name col-sm-6 label label-warning">
                        @if($ad->getUser()->company_id)
                            {{$ad->getUser()->getCompany()->name}}
                        @else
                            {{$ad->seller}}
                        @endif
                    </div>
                    <div class="date col-sm-6 label label-primary">{{$ad->created_at->format('d.m.Y')}}</div>
                </div>
                <div class="block2">
                    <div class="text">
                        <table class="table table-striped">
                            <tr>
                                <td>Тип лома:</td>
                                <td class="text-right">{{$ad->getMetal()->title}}</td>
                            </tr>
                            <tr>
                                <td>Объем:</td>
                                <td class="text-right">{{$ad->volume}} кг</td>
                            </tr>
                            <tr>
                                <td>Цена:</td>
                                <td class="text-right">{{$ad->price}} руб</td>
                            </tr>
                            <tr>
                                <td>Тип сделки:</td>
                                <td class="text-right">{{$ad->deal}}</td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td class="text-right">{{$ad->phone}}</td>
                            </tr>
                            <tr>
                                <td>{{$ad->getCity()->type}}</td>
                                <td class="text-right">{{$ad->getCity()->title}}</td>
                            </tr>
                            <tr>
                                <td class="text-center notButton" colspan="2" data-toggle="collapse"
                                    data-target="#notation-{{$ad->id}}">Примечание
                                </td>
                            </tr>
                            <tr id="notation-{{$ad->id}}" class="collapse notBody">
                                <td colspan="2">{!! $ad->text !!}</td>
                            </tr>
                        </table>

                    </div>
                </div>
                {{--<div class="block3">--}}
                {{--<div class="reiting">--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--<span class="glyphicon glyphicon-star-empty"></span>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        @empty
            <div class="label label-info"> — Объявлений нет —</div>
        @endforelse
        <hr>
        <div class="row">
            <div class="col-xs-12 text-center">{{$ads->render()}} </div>
        </div>

    </div>

@endsection