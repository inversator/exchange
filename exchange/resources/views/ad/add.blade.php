@extends('layouts.main')
@section('meta_k', 'Добавить объявление')
@section('meta_d', 'Добавить объявление')
@section('bTitle', 'Добавить объявление')
@section('topStyle')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/kladr.css') }}"/>
@endsection
@section('bottomScripts')
    <script type="text/javascript" src="/js/maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $.mask.definitions['~'] = '[+-]';
            $('#entity #phone').mask('(999) 999-9999');
            $('#individual #phone').mask('(999) 999-9999');
        });
    </script>
    <script type="text/javascript" src="/js/kladr.js"></script>
    <script>
        $(function () {
            $('#individual [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#individual #city_id').val(obj.id);
                        $('#individual #type').val(obj.type);
                        $('#individual #type_short').val(obj.typeShort);
                        $('#individual #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        $(function () {
            $('#entity [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#entity #city_id').val(obj.id);
                        $('#entity #type').val(obj.type);
                        $('#entity #type_short').val(obj.typeShort);
                        $('#entity #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        //        })
        $(document).ready(function () {

            var storage = localStorage.getItem('item');

            if (storage && storage !== "#") {
                $('.nav-tabs a[href="' + storage + '"]').tab('show');
            }

            var list = $('ul.nav').find('li');

            $('ul.nav').on('click', 'li:not(.active)', function () {
                var itemId = $(this).find('a').attr('href');
                localStorage.setItem('item', itemId);
            });
        });
    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Регистрация</div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#individual" data-toggle="tab">Физическое лицо</a></li>
                            <li><a href="#entity" data-toggle="tab">Юридическое лицо</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="individual">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/ad/') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="role" value="1">

                                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                                        <label for="login" class="col-md-4 control-label">Придумайте логин</label>

                                        <div class="col-md-6">
                                            <input id="login" type="text" class="form-control" name="login"
                                                   value="{{ old('login') }}"
                                                   placeholder="ivanov1980"
                                                   autofocus>

                                            @if ($errors->has('login'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Ваше имя</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                   value="{{ old('name') }}"
                                                   placeholder="Иван"
                                                   autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                        <label for="surname" class="col-md-4 control-label">Ваша фамилия</label>

                                        <div class="col-md-6">
                                            <input id="surname" type="text" class="form-control" name="surname"
                                                   value="{{ old('surname') }}"
                                                   placeholder="Иванов"
                                                   autofocus>

                                            @if ($errors->has('surname'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail адрес</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}"
                                                   placeholder="ivanov{{urldecode('%40')}}gmail.ru"
                                                    >

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="col-md-4 control-label">Телефон</label>

                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">+7</span>
                                                <input id="phone" type="phone" class="form-control" name="phone"
                                                       value="{{ old('phone') }}"
                                                       placeholder="(888) 888-8888 "
                                                        >
                                            </div>

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div id="cityGroup"
                                         class="form-group{{ $errors->has('city') || $errors->has('city_id') ? ' has-error' : '' }}">
                                        <label for="city" class="col-md-4 control-label">Город</label>

                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input id="city" type="text" class="form-control" name="city"
                                                       value="{{ old('city') }}"
                                                       placeholder="Москва">
                                                <input id="city_id" type="hidden" name="city_id"
                                                       value="{{ old('city_id') }}">
                                                <input id="type" type="hidden" name="type" value="{{ old('type') }}">
                                                <input id="type_short" type="hidden" name="type_short"
                                                       value="{{ old('type_short') }}">
                                                <input id="okato" type="hidden" name="okato" value="{{ old('okato') }}">
                                            </div>

                                            @if ($errors->has('city') || $errors->has('city_id'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city') ? $errors->first('city') :  $errors->first('city_id')}}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Пароль</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password"
                                                   placeholder="******">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-4 control-label">Повторите
                                            пароль</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation"
                                                   placeholder="******"
                                                    >

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="sending" name="sending"
                                                   @if(old('sending') == 'on') checked @endif/>

                                            Высылать мне
                                            информацию
                                            о
                                            стоимости
                                            металлов
                                            {{ $errors->has('sending') ? 'Формат ответа неверен' : '' }}
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Стать участником биржи
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="entity">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="role" value="2">

                                    <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                                        <label for="login" class="col-md-4 control-label">Придумайте логин</label>

                                        <div class="col-md-6">
                                            <input id="login" type="text" class="form-control" name="login"
                                                   value="{{ old('login') }}"
                                                   placeholder="ivanov1980"
                                                   autofocus>

                                            @if ($errors->has('login'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                                        <label for="company" class="col-md-4 control-label">Название компании</label>

                                        <div class="col-md-6">
                                            <input id="company" type="text" class="form-control" name="company"
                                                   value="{{ old('company') }}"
                                                   placeholder='ИП "ИВАНОВ"'
                                                   autofocus>

                                            @if ($errors->has('company'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('company') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Ваше имя</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                   value="{{ old('name') }}"
                                                   placeholder="Иван"
                                                   autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                        <label for="surname" class="col-md-4 control-label">Ваша фамилия</label>

                                        <div class="col-md-6">
                                            <input id="surname" type="text" class="form-control" name="surname"
                                                   value="{{ old('surname') }}"
                                                   placeholder="Иванов"
                                                   autofocus>

                                            @if ($errors->has('surname'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-4 control-label">E-Mail адрес</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}"
                                                   placeholder="ivanov{{urldecode('%40')}}gmail.ru"
                                                    >

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="col-md-4 control-label">Телефон</label>

                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">+7</span>
                                                <input id="phone" type="phone" class="form-control" name="phone"
                                                       value="{{ old('phone') }}"
                                                       placeholder="(888) 888-8888 "
                                                        >
                                            </div>

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div id="cityGroup"
                                         class="form-group{{ $errors->has('city') || $errors->has('city_id') ? ' has-error' : '' }}">
                                        <label for="city" class="col-md-4 control-label">Город</label>

                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <input id="city" type="text" class="form-control" name="city"
                                                       value="{{ old('city') }}"
                                                       placeholder="Москва">
                                                <input id="city_id" type="hidden" name="city_id"
                                                       value="{{ old('city_id') }}">
                                                <input id="type" type="hidden" name="type" value="{{ old('type') }}">
                                                <input id="type_short" type="hidden" name="type_short"
                                                       value="{{ old('type_short') }}">
                                                <input id="okato" type="hidden" name="okato" value="{{ old('okato') }}">
                                            </div>

                                            @if ($errors->has('city') || $errors->has('city_id'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city') ? $errors->first('city') :  $errors->first('city_id')}}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 control-label">Пароль</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password"
                                                   placeholder="******">

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password-confirm" class="col-md-4 control-label">Повторите
                                            пароль</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation"
                                                   placeholder="******"
                                                    >

                                            @if ($errors->has('password_confirmation'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="sending" name="sending"
                                                   @if(old('sending') == 'on') checked @endif/>
                                            Высылать мне
                                            информацию
                                            о
                                            стоимости
                                            металлов
                                            {{ $errors->has('sending') ? 'Формат ответа неверен' : '' }}
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Стать участником биржи
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3 id="grid-intro">Правила регистрации</h3>

                <p>Для регистрации на сайте заполните следующие поля:</p>
                <ul>
                    <li>Логин &mdash; имя Вашей учетной записи. Может быть как подлинным именем или фамилией, так и
                        вымышленным псевдонимом. <span class="text-danger">Разрешается использовать только латинские символы и цифры.</span>
                    </li>
                    <li>Email &mdash; адрес вашей текущей электронной почты. На неё будет отправлена ссылка для
                        подтверждения регистрации, а так же, в случае Вашего согласия, новостные расссылки.
                    </li>
                    <li>Пароль &mdash; введите набор знаков для подтверждения личности. <span class="text-danger">Не менее 8-ми символов!</span>
                    </li>
                    <li>Повторите пароль &mdash; введите пароль повторно. Так мы удостоверимся что вы вводили его
                        верно.
                    </li>

                </ul>
            </div>
        </div>
    </div>
@endsection
