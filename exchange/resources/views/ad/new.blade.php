@extends('layouts.main')
@section('meta_k', 'Добавить объявление')
@section('meta_d', 'Добавить объявление')
@section('bTitle', 'Добавить объявление')
@section('topStyle')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/kladr.css') }}"/>
@endsection
@section('bottomScripts')
    <script type="text/javascript" src="/js/maskedinput.js"></script>
    <script type="text/javascript">
        jQuery(function ($) {
            $.mask.definitions['~'] = '[+-]';
            $('#sale #phone').mask('(999) 999-9999');
            $('#purchase #phone').mask('(999) 999-9999');
        });
    </script>
    <script type="text/javascript" src="/js/kladr.js"></script>
    <script>
        $(function () {
            $('#sale [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#individual #city_id').val(obj.id);
                        $('#individual #type').val(obj.type);
                        $('#individual #type_short').val(obj.typeShort);
                        $('#individual #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        $(function () {
            $('#purchase [name="city"]').kladr({
                token: '57ebad7d0a69de7b3a8b4596',
                type: $.kladr.type.city,
                change: function (obj) {
                    if (obj) {
                        $('#entity #city_id').val(obj.id);
                        $('#entity #type').val(obj.type);
                        $('#entity #type_short').val(obj.typeShort);
                        $('#entity #okato').val(obj.okato);
                    }
                },
                verify: true
            });
        });
        //        })
        $(document).ready(function () {

            var storage = localStorage.getItem('item');

            if (storage && storage !== "#") {
                $('.nav-tabs a[href="' + storage + '"]').tab('show');
            }

            var list = $('ul.nav').find('li');

            $('ul.nav').on('click', 'li:not(.active)', function () {
                var itemId = $(this).find('a').attr('href');
                localStorage.setItem('item', itemId);
            });
        });
    </script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'text-sale', {
            customConfig : 'config.js',
            toolbar : [
                [ 'Source', '-', 'Bold', 'Italic' ]
            ]
        } );
        CKEDITOR.replace( 'text-purchase', {
            customConfig : 'config.js',
            toolbar : [
                [ 'Source', '-', 'Bold', 'Italic' ]
            ]
        } );
    </script>
@endsection
@section('content')
    <script src="{{ asset('/js/ckeditor/ckeditor.js') }}"
            type="text/javascript" charset="utf-8" ></script>
    <script>
        var editor = CKEDITOR.replace( 'editor1' );
    </script>
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Добавление объявления</div>
                    <div class="panel-body">

                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#sale" data-toggle="tab">На продажу</a></li>
                            <li><a href="#purchase" data-toggle="tab">На покупку</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="sale">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/ad/add') }}">
                                {{ csrf_field() }}


                                <h3>Контактные данные

                                    {{--@if($user->getRole()->name == 'entity')--}}
                                    {{--представителя компании {{ $user->getCompany()->name }}--}}
                                    {{--@else--}}
                                    {{--продавца--}}
                                    {{--@endif--}}
                                </h3>

                                <input type="hidden" id="adType" name="adType" value="1">

                                <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}">
                                    <label for="seller" class="col-md-4 control-label">Как Вас представить?</label>

                                    <div class="col-md-6">
                                        <input id="seller" type="text" class="form-control" name="seller"
                                               value="@if(old('seller')){{old('seller')}}@else <?php echo $user->name . ' ' . $user->surname; ?>@endif"
                                               placeholder="Иванов Иван"
                                               autofocus>

                                        @if ($errors->has('seller'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('seller') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('phone') ? ' has-error' : ''}}">
                                    <label for="phone" class="col-xs-4 control-label">Телефон для связи</label>

                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">+7</span>
                                            <input id="phone" type="text" class="form-control" name="phone"
                                                   value="@if(old('phone')){{old('phone')}}
                                                   @else <?php echo $user->phone; ?> @endif"
                                                   placeholder="(920) 888-88-88" readonly>
                                        </div>
                                        @if($errors->has('phone'))
                                            <span class="help-block">
                                                    <strong>{{$errors->first('phone')}}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group {{$errors->has('email') ? ' has-error' : ''}}">
                                    <label for="email" class="col-xs-4 control-label">Email для связи</label>

                                    <div class="col-xs-6">

                                        <input id="email" type="text" class="form-control" name="email"
                                               value="@if(old('email')){{old('email')}}@else<?php echo $user->email; ?>@endif"
                                               placeholder="ivanov{{urldecode('%40')}}gmail.ru" readonly>

                                        @if($errors->has('email'))
                                            <span class="help-block">
                                                    <strong>{{$errors->first('email')}}</strong>
                                                </span>
                                        @endif

                                    </div>
                                </div>


                                <div id="cityGroup"
                                     class="form-group{{ $errors->has('city') || $errors->has('city_id') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">Город</label>

                                    <div class="col-md-6">

                                        <div class="input-group">
                                            <input id="city" type="text" class="form-control" name="city"
                                                   value="{{ old('city') ? old('city') : $city->title }}"
                                                   placeholder="Москва" readonly>
                                            <input id="city_id" type="hidden" name="city_id"
                                                   value="{{ old('city_id') ? old('city_id') : $city->id }}">
                                            <input id="type" type="hidden" name="type"
                                                   value="{{ old('type') ? old('type') : $city->type }}">
                                            <input id="type_short" type="hidden" name="type_short"
                                                   value="{{ old('type_short') ? old('type_short') : $city->type_short }}">
                                            <input id="okato" type="hidden" name="okato"
                                                   value="{{ old('okato') ? old('okato') : $city->okato  }}">
                                        </div>

                                        @if ($errors->has('city') || $errors->has('city_id'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') ? $errors->first('city') :  $errors->first('city_id')}}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>
                                <h3>Данные партии</h3>

                                <div class="form-group{{ $errors->has('metal') ? ' has-error' : '' }}">
                                    <label for="metal" class="col-md-4 control-label">Выберите металл</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="metal" name="metal">
                                            @foreach($metals as $metal)
                                                <option value="{{$metal->id}}">{{$metal->title}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{$errors->has('volume') ? ' has-error' : ''}}">
                                    <label for="volume" class="col-md-4 control-label">Вес партии, кг</label>

                                    <div class="col-md-6">
                                        <input class="form-control"
                                               placeholder="100"
                                               id="volume"
                                               name="volume"
                                               value="{{ Request::old('volume') }}"
                                                />
                                        @if($errors->has('volume'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('volume') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{$errors->has('price') ? ' has-error' : ''}}">
                                    <label for="price" class="col-md-4 control-label">Цена за партию, руб</label>

                                    <div class="col-md-6">
                                        <input class="form-control"
                                               placeholder="100"
                                               id="price"
                                               name="price"
                                               value="{{ old('price') }}"
                                                />
                                        @if($errors->has('price'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('price') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('deal') ? ' has-error' : '' }}">
                                    <label for="deal" class="col-md-4 control-label">Тип сделки</label>

                                    <div class="col-md-6">
                                        <select class="form-control" id="deal" name="deal">
                                            <option value="1">Предоплата</option>
                                            <option value="2">Постоплата</option>
                                            <option value="3">По договоренности</option>
                                        </select>
                                        @if ($errors->has('deal'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('deal') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{$errors->has('text') ? ' has-error' : ''}}">
                                    <label for="text" class="col-md-4 control-label">Примечание</label>

                                    <div class="col-md-6">
                                            <textarea id="text-sale" class="form-control"
                                                      placeholder="опишите что считаете важным"
                                                      {{--id="text"--}}
                                                      name="text"
                                                    >{{ old('text') }}</textarea>
                                        @if($errors->has('text'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('text') }}</strong>
                                                </span>
                                        @endif

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Разместить объявление
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                            <div class="tab-pane" id="purchase">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/ad/add') }}">
                                    {{ csrf_field() }}


                                    <h3>Контактные данные

                                        {{--@if($user->getRole()->name == 'entity')--}}
                                        {{--представителя компании {{ $user->getCompany()->name }}--}}
                                        {{--@else--}}
                                        {{--продавца--}}
                                        {{--@endif--}}
                                    </h3>
                                    <input type="hidden" id="adType" name="adType" value="2">

                                    <div class="form-group{{ $errors->has('seller') ? ' has-error' : '' }}">
                                        <label for="seller" class="col-md-4 control-label">Как Вас представить?</label>

                                        <div class="col-md-6">
                                            <input id="seller" type="text" class="form-control" name="seller"
                                                   value="@if(old('seller')){{old('seller')}}@else <?php echo $user->name . ' ' . $user->surname; ?>@endif"
                                                   placeholder="Иванов Иван"
                                                   autofocus>

                                            @if ($errors->has('seller'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('seller') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('phone') ? ' has-error' : ''}}">
                                        <label for="phone" class="col-xs-4 control-label">Телефон для связи</label>

                                        <div class="col-xs-6">
                                            <div class="input-group">
                                                <span class="input-group-addon">+7</span>
                                                <input id="phone" type="text" class="form-control" name="phone"
                                                       value="@if(old('phone')){{old('phone')}}
                                                       @else <?php echo $user->phone; ?> @endif"
                                                       placeholder="(920) 888-88-88" readonly>
                                            </div>
                                            @if($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{$errors->first('phone')}}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{$errors->has('email') ? ' has-error' : ''}}">
                                        <label for="email" class="col-xs-4 control-label">Email для связи</label>

                                        <div class="col-xs-6">

                                            <input id="email" type="text" class="form-control" name="email"
                                                   value="@if(old('email')){{old('email')}}@else<?php echo $user->email; ?>@endif"
                                                   placeholder="ivanov{{urldecode('%40')}}gmail.ru" readonly>

                                            @if($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{$errors->first('email')}}</strong>
                                                </span>
                                            @endif

                                        </div>
                                    </div>


                                    <div id="cityGroup"
                                         class="form-group{{ $errors->has('city') || $errors->has('city_id') ? ' has-error' : '' }}">
                                        <label for="city" class="col-md-4 control-label">Город</label>

                                        <div class="col-md-6">

                                            <div class="input-group">
                                                <input id="city" type="text" class="form-control" name="city"
                                                       value="{{ old('city') ? old('city') : $city->title }}"
                                                       placeholder="Москва" readonly>
                                                <input id="city_id" type="hidden" name="city_id"
                                                       value="{{ old('city_id') ? old('city_id') : $city->id }}">
                                                <input id="type" type="hidden" name="type"
                                                       value="{{ old('type') ? old('type') : $city->type }}">
                                                <input id="type_short" type="hidden" name="type_short"
                                                       value="{{ old('type_short') ? old('type_short') : $city->type_short }}">
                                                <input id="okato" type="hidden" name="okato"
                                                       value="{{ old('okato') ? old('okato') : $city->okato  }}">
                                            </div>

                                            @if ($errors->has('city') || $errors->has('city_id'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('city') ? $errors->first('city') :  $errors->first('city_id')}}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <hr>
                                    <h3>Данные партии</h3>

                                    <div class="form-group{{ $errors->has('metal') ? ' has-error' : '' }}">
                                        <label for="metal" class="col-md-4 control-label">Выберите металл</label>

                                        <div class="col-md-6">
                                            <select class="form-control" id="metal" name="metal">

                                                @foreach($metals as $metal)
                                                    @if(Request::old('metal') == $metal->id)
                                                        <option value="{{$metal->id}}" selected>{{$metal->title}}</option>
                                                    @else
                                                    <option value="{{$metal->id}}">{{$metal->title}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{$errors->has('volume') ? ' has-error' : ''}}">
                                        <label for="volume" class="col-md-4 control-label">Вес партии, кг</label>

                                        <div class="col-md-6">
                                            <input class="form-control"
                                                   placeholder="100"
                                                   id="volume"
                                                   name="volume"
                                                   value="{{ Request::old('volume') }}"
                                                    />
                                            @if($errors->has('volume'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('volume') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{$errors->has('price') ? ' has-error' : ''}}">
                                        <label for="price" class="col-md-4 control-label">Цена за партию, руб</label>

                                        <div class="col-md-6">
                                            <input class="form-control"
                                                   placeholder="100"
                                                   id="price"
                                                   name="price"
                                                   value="{{ old('price') }}"
                                                    />
                                            @if($errors->has('price'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('price') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('deal') ? ' has-error' : '' }}">
                                        <label for="deal" class="col-md-4 control-label">Тип сделки</label>

                                        <div class="col-md-6">
                                            <select class="form-control" id="deal" name="deal">
                                                <option value="1">Предоплата</option>
                                                <option value="2">Постоплата</option>
                                                <option value="3">По договоренности</option>
                                            </select>
                                            @if ($errors->has('deal'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('deal') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{$errors->has('text') ? ' has-error' : ''}}">
                                        <label for="text" class="col-md-4 control-label">Примечание</label>

                                        <div class="col-md-6">
                                            <textarea class="form-control"
                                                      placeholder="опишите что считаете важным"
                                                      id="text-purchase"
                                                      name="text"
                                                    >{{ old('text') }}</textarea>
                                            @if($errors->has('text'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('text') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Разместить объявление
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <h3 id="grid-intro">Правила размещения объявления</h3>
                <ul>
                    <li> Разрешается иметь только одну учетную запись;</li>
                    <li>Пользователи биржи Лом.ру самостоятельно размещают объявления и несут ответственность за то, что размещенная информация соответствую правилам биржи и законодательству РФ.</li>
                    <li>Не размещайте одно и тоже объявление несколько раз;</li>
                    <li>Нельзя размещать запрещенную номенклатуру товаров;</li>
                    <li>Запрещается использование ненормативной лексики;</li>
                    <li>Размещайте объявления только о конкретном товаре;</li>
                    <li>Запрещено размещение объявлени рекламного характера;</li>
                    <li>Запрещается использовать программы автозагрузки для размещения объявления без соглашения с Лом.ру.</li>
                    <li>Недопустимо наличие в объявлении или учетной записи пользователя эротического, порнографического, экстремистского или иного содержания;</li>
                    <li>Имя в объявлении должно соответствовать действительности;</li>
                    <li>Поля раздела Данные партии обязательны к заполнению;</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
