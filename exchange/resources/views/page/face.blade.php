@extends('layouts.main')

@section('bTitle', $bTitle)

@section('content')
    <div class="row">
        <p class="pull-right visible-xs hideBut">
            {{--<button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Услуги</button>--}}
        </p>

        <div class="col-xs-12">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-12 col-sm-9">
                    @include('blocks.slider')
                </div>
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                    <div class="list-group">
                        <h2>Добавление объявлений</h2>

                        <p>
                            Для размещения объявлений, зарегистрируйтесь и авторизуйтесь на сайте. Затем нажмите на ваш
                            логин и выберите пункт добавить объявление.
                        </p>

                        <p>
                            Если вы представляете организацию выберите вид регистрации "юридическое лицо".
                        </p>

                        <h3>Удачных сделок!</h3>
                        {{--<a href="#" class="list-group-item">Свинец<span--}}
                        {{--class="pull-right label label-info">До 90 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Медь<span--}}
                        {{--class="pull-right label label-info">До 290 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Аллюминиий<span class="pull-right label label-info">До 90 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Латунь<span class="pull-right label label-info">До 155 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Нержавейка<span class="pull-right label label-info">До 53 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Бронза<span class="pull-right label label-info">До 200 руб/кг</span></a>--}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6"><h2>КУПИТЬ ЛОМ МЕТАЛЛА</h2></div>
                <div class="col-xs-12 col-sm-6">
                </div>
            </div>

            <div class="row mainLineup">
                @forelse($ads as $ad)
                    <div class="ad col-sm-4">
                        <div class="block1">
                            <div class="name col-sm-6 label label-warning">
                                @if($ad->getUser()->company_id)
                                    {{$ad->getUser()->getCompany()->name}}
                                @else
                                    {{$ad->seller}}
                                @endif
                            </div>
                            <div class="date col-sm-6 label label-primary">{{$ad->created_at->format('d.m.Y')}}</div>
                        </div>
                        <div class="block2">
                            <div class="text">
                                <table class="table table-striped">
                                    <tr>
                                        <td>Тип лома:</td>
                                        <td class="text-right">{{$ad->getMetal()->title}}</td>
                                    </tr>
                                    <tr>
                                        <td>Объем:</td>
                                        <td class="text-right">{{$ad->volume}} кг</td>
                                    </tr>
                                    <tr>
                                        <td>Цена:</td>
                                        <td class="text-right">{{$ad->price}} руб</td>
                                    </tr>
                                    <tr>
                                        <td>Тип сделки:</td>
                                        <td class="text-right">{{$ad->deal}}</td>
                                    </tr>
                                    <tr>
                                        <td>Телефон</td>
                                        <td class="text-right">{{$ad->phone}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$ad->getCity()->type}}</td>
                                        <td class="text-right">{{$ad->getCity()->title}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center notButton" colspan="2" data-toggle="collapse"
                                            data-target="#notation-{{$ad->id}}">Примечание
                                        </td>
                                    </tr>
                                    <tr id="notation-{{$ad->id}}" class="collapse notBody">
                                        <td colspan="2">{!! $ad->text !!}</td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                @empty
                    <div class="label label-info"> — Объявлений нет —</div>
                @endforelse

                {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
                {{--<div class="row center-block">--}}
                {{--<a href="#">--}}
                {{--<img src="/img/img03.jpg" alt=""/>--}}
                {{--<!--                                    <span class="modelName">DAILY</span>-->--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
                {{--<div class="row center-block">--}}
                {{--<a href="#">--}}
                {{--<img src="/img/img04.jpg" alt=""/>--}}
                {{--<!--                                    <span class="modelName">EUROCARGO</span>-->--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
                {{--<div class="row center-block">--}}
                {{--<a href="#">--}}
                {{--<img src="/img/img05.jpg" alt=""/>--}}
                {{--<!--                                    <span class="modelName">STRALIS</span>-->--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
                {{--<div class="row center-block">--}}
                {{--<a href="#">--}}
                {{--<img src="/img/img06.jpg" alt=""/>--}}
                {{--<!--                                    <span class="modelName">TRAKKER</span>-->--}}
                {{--</a>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="text-center">
                <div class="btn btn-success" onclick="location.href='/ads/sale'">Смотреть все
                    объявления
                </div>
            </div>


            {{--<div class="row mainLineup">--}}
            {{--<h3> — ЧЕРНЫЙ — </h3>--}}

            {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
            {{--<div class="row center-block">--}}
            {{--<a href="#">--}}
            {{--<img src="/img/img07.jpg" alt=""/>--}}
            {{--<!--                                    <span class="modelName">DAILY</span>-->--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
            {{--<div class="row center-block">--}}
            {{--<a href="#">--}}
            {{--<img src="/img/img08.jpg" alt=""/>--}}
            {{--<!--                                    <span class="modelName">EUROCARGO</span>-->--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
            {{--<div class="row center-block">--}}
            {{--<a href="#">--}}
            {{--<img src="/img/img09.jpg" alt=""/>--}}
            {{--<!--                                    <span class="modelName">STRALIS</span>-->--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
            {{--<div class="row center-block">--}}
            {{--<a href="#">--}}
            {{--<img src="/img/img10.jpg" alt=""/>--}}
            {{--<!--                                    <span class="modelName">TRAKKER</span>-->--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            <!-- end model block-->
            <hr>
            {{--<div class="h2">РЕЙТИНГИ</div>--}}
            {{--<div class="row mainAuto">--}}
            {{--<div class="cols-xs-12 col-sm-6">--}}
            {{--<h4>Покупка</h4>--}}
            {{--<table class="table table-striped table-hover">--}}
            {{--<tr>--}}
            {{--<th>Название предприятия</th>--}}
            {{--<th>Объемы (тонны)</th>--}}
            {{--</tr>--}}
            {{--<tr class="danger">--}}
            {{--<td>Рязанский завод цветных металлов</td>--}}
            {{--<td>12 000</td>--}}
            {{--</tr>--}}
            {{--<tr class="warning">--}}
            {{--<td>ОАО "Волгоградский завод тракторных деталей и нормалей"</td>--}}
            {{--<td>10 700</td>--}}
            {{--</tr>--}}
            {{--<tr class="info">--}}
            {{--<td>ОАО "Белебеевский завод "Автонормаль"</td>--}}
            {{--<td>8 400</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ОАО "Челябинский кузнечно-прессовый завод"</td>--}}
            {{--<td>6 000</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ЗАО "66 Металлообрабатывающий завод"</td>--}}
            {{--<td>3 700</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ОАО "Завод "Сельмаш"</td>--}}
            {{--<td>1 700</td>--}}
            {{--</tr>--}}
            {{--</table>--}}

            {{--</div>--}}
            {{--<div class="cols-xs-12 col-sm-6">--}}
            {{--<h4>Продажа</h4>--}}
            {{--<table class="table table-striped table-hover">--}}
            {{--<tr>--}}
            {{--<th>Название предприятия</th>--}}
            {{--<th>Объемы (тонны)</th>--}}
            {{--</tr>--}}
            {{--<tr class="danger">--}}
            {{--<td>Рязанский завод цветных металлов</td>--}}
            {{--<td>12 000</td>--}}
            {{--</tr>--}}
            {{--<tr class="warning">--}}
            {{--<td>ОАО "Волгоградский завод тракторных деталей и нормалей"</td>--}}
            {{--<td>10 700</td>--}}
            {{--</tr>--}}
            {{--<tr class="info">--}}
            {{--<td>ОАО "Белебеевский завод "Автонормаль"</td>--}}
            {{--<td>8 400</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ОАО "Челябинский кузнечно-прессовый завод"</td>--}}
            {{--<td>6 000</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ЗАО "66 Металлообрабатывающий завод"</td>--}}
            {{--<td>3 700</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
            {{--<td>ОАО "Завод "Сельмаш"</td>--}}
            {{--<td>1 700</td>--}}
            {{--</tr>--}}
            {{--</table>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="jumbotron">
                <h1>Лом цветных металлов. Прием лома металла</h1>

                <p>
                    Приоритетным направлением деятельности компании «Лом.ру» является помощь клиентам в закупке и
                    реализации лома цветных
                    металлов. На каждый имеющийся у вас вид металла у нас найдутся покупатели, а если покупатель — Вы,
                    то продавцы.
                </p>


            </div>
            <div id="bottomContent">

<h3> Почему именно Лом.ру?</h3>
                <p>
                    Потому что у нас есть все необходимое для реализации лома цветных и черных металлов. Здесь Вы получаете возможность отслеживать объявления из любой точки страны, осуществлять торговлю без посредников и «маржи», тем самым ускоряя процесс торговли.
                </p>
                <p>Размещаемые Вами объявления контроллируются через административную панель сайта.</p>
            </div>
            <!--END TEXT-->
            {{--<div class="h1">Последние новости</div>--}}
            <div class="mainNews row">
                {{--<div class="col-xs-12">--}}
                {{--<div class="row">--}}
                {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                {{--<img src="/img/f04e16fc836ad1b595c4f41a4aca2ff2.jpg" alt=""/>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                {{--<div class="label label-success">23.11.2015</div>--}}
                {{--<a class="h3" href="/about/news/3044/">Подготовка 11-го Металл-Феста в разгаре</a>--}}

                {{--<div>Уже совсем скоро, сразу после знойного лета, состоится традиционный ежегодный (уже XI)--}}
                {{--Металл-Фест, на который соберутся члены Российского союза поставщиков металлопродукции--}}
                {{--(РСПМ) из различных регионов нашей необъятной Родины: Москвы и Московской области,--}}
                {{--Татарстана, Башкортостана, Нижегородской, Самарской областей и др.--}}
                {{--</div>--}}
                {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<hr>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12">--}}
                {{--<div class="row">--}}
                {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                {{--<img src="/img/969e00ee9591587e517d9d17e634f569.jpg" alt=""/>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                {{--<div class="label label-success">23.11.2015</div>--}}
                {{--<a class="h3" href="/about/news/3038/">В США установлены окончательные пошлины на российский--}}
                {{--холоднокатаный прокат</a>--}}

                {{--<div>Министерство торговли США установило окончательные антидемпинговые и компенсационные--}}
                {{--пошлины на холоднокатаный прокат из России и еще четырех стран — Бразилии,--}}
                {{--Великобритании, Индии и Южной Кореи. Ранее такие же пошлины были введены на аналогичную--}}
                {{--продукцию китайского и японского производства.--}}
                {{--</div>--}}
                {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<hr>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12">--}}
                {{--<div class="row">--}}
                {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                {{--<img src="/img/4ecbb0db00b5174ec79388373d8ae6b1.jpg" alt=""/>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                {{--<div class="label label-success">23.11.2015</div>--}}
                {{--<a class="h3" href="/about/news/3035/">Коршуновский ГОК увеличил производство железорудного--}}
                {{--концентрата</a>--}}

                {{--<div>Коршуновский ГОК (входит в Группу Мечел ) увеличил выпуск железорудного концентрата на--}}
                {{--6 % по сравнению с первым полугодием прошлого года. За шесть месяцев 2016 г. на--}}
                {{--обогатительной фабрике комбината было произведено 1 328,3 тыс. т железорудного--}}
                {{--концентрата, что на 80 тыс. т больше, чем за аналогичный период прошлого года.--}}
                {{--</div>--}}
                {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<hr>--}}
                {{--</div>--}}


            </div>
            <!-- END NEWS -->

        </div>
        <!-- END CONTENT -->


    </div>
@stop
