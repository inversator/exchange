@extends('layouts.main')
@section('meta_k', $page['meta_k'])
@section('meta_d', $page['meta_d'])
@section('bTitle', $page['bTitle'])

@section('content')
    <div class="row">
        <p class="pull-right visible-xs hideBut">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Услуги</button>
        </p>

        <div class="col-xs-12">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-12 col-sm-9">
                    <div class="jumbotron">
                        <h1>{{$page['pTitle']}}</h1>
                        {!!$page['desc']!!}
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                    <div class="list-group">
                        <h2>Добавление объявлений</h2>

                        <p>
                            Для размещения объявлений, зарегистрируйтесь и авторизуйтесь на сайте. Затем нажмите на ваш
                            логин и выберите пункт добавить объявление.
                        </p>
                        <p>
                            Если вы представляете организацию выберите вид регистрации "юридическое лицо".
                        </p>
                        {{--<h3>ПРОДАТЬ ЛОМ</h3>--}}
                        {{--<a href="#" class="list-group-item">Свинец<span--}}
                                    {{--class="pull-right label label-info">До 90 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Медь<span--}}
                                    {{--class="pull-right label label-info">До 290 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Аллюминиий<span class="pull-right label label-info">До 90 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Латунь<span class="pull-right label label-info">До 155 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Нержавейка<span class="pull-right label label-info">До 53 руб/кг</span></a>--}}
                        {{--<a href="#" class="list-group-item">Бронза<span class="pull-right label label-info">До 200 руб/кг</span></a>--}}
                    </div>
                </div>
            </div>

            <div id="bottomContent">
                {!! $page['text'] !!}
            </div>
            <!--END TEXT-->
            {{--<div class="h1">Последние новости</div>--}}
            {{--<div class="mainNews row">--}}
                {{--<div class="col-xs-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                            {{--<img src="/img/f04e16fc836ad1b595c4f41a4aca2ff2.jpg" alt=""/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                            {{--<div class="label label-success">23.11.2015</div>--}}
                            {{--<a class="h3" href="/about/news/3044/">Подготовка 11-го Металл-Феста в разгаре</a>--}}

                            {{--<div>Уже совсем скоро, сразу после знойного лета, состоится традиционный ежегодный (уже XI)--}}
                                {{--Металл-Фест, на который соберутся члены Российского союза поставщиков металлопродукции--}}
                                {{--(РСПМ) из различных регионов нашей необъятной Родины: Москвы и Московской области,--}}
                                {{--Татарстана, Башкортостана, Нижегородской, Самарской областей и др.--}}
                            {{--</div>--}}
                            {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                            {{--<img src="/img/969e00ee9591587e517d9d17e634f569.jpg" alt=""/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                            {{--<div class="label label-success">23.11.2015</div>--}}
                            {{--<a class="h3" href="/about/news/3038/">В США установлены окончательные пошлины на российский--}}
                                {{--холоднокатаный прокат</a>--}}

                            {{--<div>Министерство торговли США установило окончательные антидемпинговые и компенсационные--}}
                                {{--пошлины на холоднокатаный прокат из России и еще четырех стран — Бразилии,--}}
                                {{--Великобритании, Индии и Южной Кореи. Ранее такие же пошлины были введены на аналогичную--}}
                                {{--продукцию китайского и японского производства.--}}
                            {{--</div>--}}
                            {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                {{--</div>--}}

                {{--<div class="col-xs-12">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-12 col-sm-3 col-md-2 img">--}}
                            {{--<img src="/img/4ecbb0db00b5174ec79388373d8ae6b1.jpg" alt=""/>--}}
                        {{--</div>--}}
                        {{--<div class="col-xs-12 col-sm-9 col-md-10 text">--}}
                            {{--<div class="label label-success">23.11.2015</div>--}}
                            {{--<a class="h3" href="/about/news/3035/">Коршуновский ГОК увеличил производство железорудного--}}
                                {{--концентрата</a>--}}

                            {{--<div>Коршуновский ГОК (входит в Группу Мечел ) увеличил выпуск железорудного концентрата на--}}
                                {{--6 % по сравнению с первым полугодием прошлого года. За шесть месяцев 2016 г. на--}}
                                {{--обогатительной фабрике комбината было произведено 1 328,3 тыс. т железорудного--}}
                                {{--концентрата, что на 80 тыс. т больше, чем за аналогичный период прошлого года.--}}
                            {{--</div>--}}
                            {{--<a href="/about/news/3044/">Подробнее >></a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                {{--</div>--}}


            {{--</div>--}}
            <!-- END NEWS -->

        </div>
        <!-- END CONTENT -->


    </div>
@stop
