@extends('layouts.main')
@section('meta_k', $page['meta_k'])
@section('meta_d', $page['meta_d'])
@section('bTitle', $page['bTitle'])

@section('content')
    <div class="row">
        <p class="pull-right visible-xs hideBut">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Услуги</button>
        </p>

        <div class="col-xs-12">

            <div id="bottomContent">
                <div class="panel-group" id="metals">
                    @forelse($metals as $metal)

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#metals" href="#collapse{{$metal->id}}">
                                        {{$metal->title}}</a>
                                </h4>
                            </div>
                            <div id="collapse{{$metal->id}}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-group" id="types">
                                        <?php $types = $metal->types()->get(); ?>
                                        @forelse($types as $type)
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#types"
                                                       href="#collapse-type{{$metal->id}}-{{$type->id}}">
                                                        {{$metal->title}} {{$type->title}}</a>
                                                </h4>
                                            </div>
                                            <div id="collapse-type{{$metal->id}}-{{$type->id}}" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    {!!  $type->desc!!}
                                                </div>
                                            </div>

                                        @empty
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>

                    @empty
                        <p> - Нет металлов - </p>
                    @endforelse

                </div>

            </div>

            <!--END TEXT-->

            <!-- END NEWS -->

        </div>
        <!-- END CONTENT -->


    </div>
@stop
