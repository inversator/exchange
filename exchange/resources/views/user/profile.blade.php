@extends('layouts.main')

@section('bTitle', $bTitle)

@section('content')
    <div class="row" id="profile">
        <p class="pull-right visible-xs hideBut">
            <button type="button" class="btn btn-primary btn-xs" data-toggle="offcanvas">Услуги</button>
        </p>

        <div class="col-xs-12">
            <div class="row row-offcanvas row-offcanvas-right">
                <div class="col-xs-12 col-sm-9">
                    <div class="jumbotron">
                        <div class="row">
                            <h1>Личный кабинет</h1>

                            <h2 class="col-xs-6">{{Auth::user()->login}}</h2>

                            <div class="col-xs-6">
                                <p>{{Auth::user()->email}}</p>

                                <p>{{\App\User::phoneFormat(Auth::user()->phone)}}</p>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
                    <div class="list-group">
                        <h3>{{Auth::user()->getRole()->alias}}</h3>
                        {{Auth::user()->surname . " " . Auth::user()->name}}
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="name">

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection