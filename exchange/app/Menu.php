<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';

    public function getNavBar()
    {
        return $this->published()->orderBy('position')->get();
    }

    public function scopePublished($query)
    {
        $query->where(['active'=>'1']);
    }
}
