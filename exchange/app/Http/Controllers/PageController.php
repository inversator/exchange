<?php

/**
 * Created by PhpStorm.
 * User: maa
 * Date: 22.09.2016
 * Time: 10:56
 */
namespace App\Http\Controllers;

use App\Metal;
use App\MetalType;
use App\Page;
use App\Slider;
use App\Ad;

class PageController extends MainController
{

    public function __constructor()
    {
        parent::__construct();
    }

    public function index()
    {

        $bTitle = 'Лом.ру: Торговая площадка для черных и цветных металлов | Лом, черный лом металлов, аналитика, статистика, цены,
        прогнозы';

        return view('page.face', ['bTitle' => $bTitle, 'ads' => Ad::where('adType', '=', '2')->orderBy('created_at')->limit(6)->get()]);
    }

    public function item($slug)
    {
        $modelPage = new Page();
        $thisPage = $modelPage->getByUrl($slug);

        return view('page.item', ['page' => $thisPage]);

    }

    public function gosts()
    {

        $page['meta_k'] = 'ГОСТы';
        $page['meta_d'] = 'ГОСТы';
        $page['bTitle'] = 'ГОСТы';
        $page['pTitle'] = 'ГОСТы';
        $page['desc'] = '123';

        return view('page.gosts', ['metals' => Metal::orderBy('title')->get(), 'page' => $page]);

    }

}