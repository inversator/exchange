<?php

namespace App\Http\Controllers\Auth;

use App\Company;
use App\UserRole;
use App\User;
use App\City;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validation = Validator::make($data, [
            'login' => 'required|max:255|min:3|unique:users|alpha_num',
            'name' => 'required|max:30|min:2|alpha',
            'surname' => 'max:30|min:2|alpha',
            'phone' => 'phone',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8|confirmed',
            'city_id' => 'required|integer',
            'city' => 'required|max:255|min:2|company',
            'type' => 'required|max:255|company',
            'type_short' => 'required|alpha|max:10',
            'okato' => 'required|numeric',
            'sending' => 'in:on,',
            'company' => 'sometimes|required_if:role,company|max:30|company',
            'role' => 'in:1,2'
        ]);
        $attributeNames = [
            'login' => 'Логин',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'phone' => 'Телефон',
            'email' => 'Email',
            'password' => 'Пароль',
            'password_confirmation' => 'Подтверждение пароля',
            'city' => 'Город',
            'city_id' => 'Город',
            'company' => 'Компания',
            'role' => 'Уровень доступа'
        ];
        $validation->setAttributeNames($attributeNames);
        $validation->setCustomMessages([
            'city_id.required' => 'Город не найден в базе, воспользуйтесь выпадающим списком',
            'email.unique' => 'Такой адрес уже используется.',
            'company.required_if' => 'Для компании необходимо указать её наименование',
            'company.company' => 'В названии компании присутствует недопустимый символ',
            'role.in' => 'Некорректный уровень доступа'
        ]);

        return $validation;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {

        if ($data['role'] == 2) {
            $createdCompany = Company::create([
                'name' => $data['company']
            ]);
        }

        $userCreate = User::create([
            'login' => $data['login'],
            'name' => $data['name'],
            'surname' => $data['surname'],
            'phone' => preg_replace('/[^\d]/', '', $data['phone']),
            'email' => $data['email'],
            'city_id' => $data['city_id'],
            'password' => bcrypt($data['password']),
            'sending' => (isset($data['sending']) and ($data['sending'] == 'on')) ? TRUE : FALSE,
            'company_id' => (isset($createdCompany)) ? $createdCompany->id : 0
        ]);

        City::create([
            'id' => $data['city_id'],
            'title' => $data['city'],
            'type' => $data['type'],
            'type_short' => $data['type_short'],
            'okato' => $data['okato']
        ]);
        UserRole::create([
            'user_id' => $userCreate->id,
            'role_id' => $data['role']
        ]);

        return $userCreate;

    }
}
