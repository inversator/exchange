<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 18.10.2016
 * Time: 13:08
 */

namespace App\Http\Controllers;


use App\Metal;
use App\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Validator;

class AdController extends MainController
{
    public function newAd()
    {
        $metals = new Metal();

        return view('ad.new', [
            'user' => \Auth::user(),
            'city' => \Auth::user()->getCity(),
            'metals' => $metals->orderBy('title')->get()
        ]);
    }

    public function adsList()
    {

        return view('ad.list', [
            'ads' => Ad::where('user_id', '=', Auth::user()->id)->get()
        ]);
    }


    public function deleteAd(Request $request)
    {
        Ad::destroy($request->route('id'));
        return redirect()->route('adList');

    }

    public function adsSale()
    {
        return view('ad.sale', [
            'ads' => Ad::where('adType','=','2')->orderBy('created_at')->paginate(9)
        ]);
    }

    public function adsPurchase()
    {
        return view('ad.purchase', [
            'ads' => Ad::where('adType','=','1')->orderBy('created_at')->paginate(9)
        ]);
    }

    public function addAd(Request $request)
    {
        $attributeNames = [
            'adType' => 'РўРёРї РѕР±СЉСЏРІР»РµРЅРёСЏ',
            'seller' => 'РџСЂРѕРґР°РІРµС†',
            'phone' => 'РўРµР»РµС„РѕРЅ',
            'email' => 'Email',

            'city' => 'Город',
            'city_id' => 'Р�РґРµРЅС‚РёС„РёРєР°С‚РѕСЂ РіРѕСЂРѕРґР°',

            'metal' => 'РўРёРї Р»РѕРјР°',
            'volume' => 'Вес партии',
            'price' => 'Цена за партию',
            'deal' => 'Тип сделки',
            'text' => 'Примечание'
        ];

        $v = Validator::make($request->all(), [

            'adType' => 'in:1,2',
            'seller' => 'required|company',
            'phone' => 'phone',
            'email' => 'required|email|max:255',

            'city' => 'required|max:255|min:2|company',
            'city_id' => 'required|integer',

            'type' => 'required|max:255|company',
            'type_short' => 'required|company|max:10',
            'okato' => 'required|integer',

            'metal' => 'required|integer',
            'volume' => 'numeric|required',
            'price' => 'numeric|required',
            'deal' => 'required|integer',


        ]);

        $v->setAttributeNames($attributeNames);
        $v->setCustomMessages([
            'city_id.required' => 'Р“РѕСЂРѕРґ РЅРµ РЅР°Р№РґРµРЅ РІ Р±Р°Р·Рµ, РІРѕСЃРїРѕР»СЊР·СѓР№С‚РµСЃСЊ РІС‹РїР°РґР°СЋС‰РёРј СЃРїРёСЃРєРѕРј',
            'email.unique' => 'РўР°РєРѕР№ Р°РґСЂРµСЃ СѓР¶Рµ РёСЃРїРѕР»СЊР·СѓРµС‚СЃСЏ.',
            'company.required_if' => 'Р”Р»СЏ РєРѕРјРїР°РЅРёРё РЅРµРѕР±С…РѕРґРёРјРѕ СѓРєР°Р·Р°С‚СЊ РµС‘ РЅР°РёРјРµРЅРѕРІР°РЅРёРµ',
            'company.company' => 'Р’ РЅР°Р·РІР°РЅРёРё РєРѕРјРїР°РЅРёРё РїСЂРёСЃСѓС‚СЃС‚РІСѓРµС‚ РЅРµРґРѕРїСѓСЃС‚РёРјС‹Р№ СЃРёРјРІРѕР»',
            'role.in' => 'РќРµРєРѕСЂСЂРµРєС‚РЅС‹Р№ СѓСЂРѕРІРµРЅСЊ РґРѕСЃС‚СѓРїР°',
            'text.text' => 'Недопустимый символ в тексте'
        ]);

        $param = ['errors' => $v->errors()];

        // if isset no-valid fields do return with errors messages
        if ($v->fails()) {
            return redirect()->back()->with($param)->withInput();
        }

        $newAd = Ad::create([
            'user_id' => Auth::user()->id,

            'adType' => $request->input('adType'),
            'seller' => $request->input('seller'),
            'email' => $request->input('email'),
            'phone' => preg_replace('/[^\d]/', '', $request->input('phone')),
            'city_id' => $request->input('city_id'),
            'metal' => $request->input('metal'),
            'volume' => $request->input('volume'),
            'price' => $request->input('price'),
            'deal' => $request->input('deal'),
            'text' => $request->input('text')
        ]);


        if ($newAd) {
            return redirect('/ad/list');
        }
    }

}