<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 10.10.2016
 * Time: 10:17
 */

namespace App\Http\Controllers;

use App\User;
use App\UserRole;

class UserController extends MainController
{
    public function __constructor()
    {
        parent::__construct();
    }

    public function profile()
    {
        $bTitle = 'Личный кабинет';
        return view('user.profile', compact('bTitle'));
    }

}