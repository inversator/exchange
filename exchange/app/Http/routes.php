<?php

/*
|--------------------------------------------------------------------------
| application routes
|--------------------------------------------------------------------------
|
| here is where you can register all of the routes for an application.
| it's a breeze. simply tell laravel the uris it should respond to
| and give it the controller to call when that uri is requested.
|
*/

View::composer('blocks.navbar', function ($view) {
    $view->with('items', app('App\Menu')->getNavBar());
});
View::composer('blocks.slider', function ($view) {
    $view->with('items', app('App\Slider')->getActive());
});

Route::any('gosts', 'PageController@gosts');

Route::any('ads/sale', 'AdController@adsSale');
Route::any('ads/purchase', 'AdController@adsPurchase');

Route::auth();

Route::get('/', 'PageController@index');
Route::get('profile', 'UserController@profile')->middleware('auth');

Route::post('ad/add', 'AdController@addAd')->middleware('auth');
Route::get('ad/add', 'AdController@newAd')->middleware('auth');
Route::get('ad/new', 'AdController@newAd')->middleware('auth');
Route::any('ad/list', 'AdController@adsList')->middleware('auth')->name('adList');

Route::any('ad/delete/{id}', 'AdController@deleteAd')->middleware('auth')->where('id', '[0-9]+');

// All unannounced routed get here and are considered pages
Route::get('/{slug}', 'PageController@item');

Event::listen('404', function () {
    return Response::error('404');
});
