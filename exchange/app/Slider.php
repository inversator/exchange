<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slides';

    public function getActive()
    {
        return $this->orderBy('position')
            ->published()
            ->get();
    }

    public function scopePublished($query)
    {
        return $query->where(['off' => false]);
    }
}
