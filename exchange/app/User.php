<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'surname',
        'login',
        'phone',
        'city',
        'city_id',
        'sending',
        'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo('App\City');
    }

    public function test()
    {
        return 'test';
    }

    public static function phoneFormat($number)
    {
        if(strlen($number) == 10) {
            return "+7 (" . substr($number, 0, 3) . ") " . substr($number, 3, 3) . "-". substr($number, 6, 2) . "-" . substr($number, 8, 2);
        } else {
            return $number;
        }
    }

    public function isAdmin()
    {
        $rights = $this->hasManyThrough('App\Role','App\UserRole', 'user_id','id','id')->get();

        foreach($rights as $right)
        {
            if($right->id == 3) return true;
        }

        return false;
    }

    public function getRole()
    {
        return $this->hasManyThrough('App\Role','App\UserRole', 'user_id','id','id')->first();
    }

    public function getCompany()
    {
        return $this->hasOne('App\Company','id','company_id')->first();
    }

    public function getCity()
    {
        return $this->hasOne('App\City','id','city_id')->first();
    }
}
