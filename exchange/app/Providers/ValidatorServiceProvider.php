<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 27.09.2016
 * Time: 15:19
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['validator']->extend('phone', function ($attribute, $value, $parameters) {

            $lengths = array(7, 10, 11);

            // Remove all non-digit characters from the number
            $number = preg_replace('/\D+/', '', $value);

            // Check if the number is within range
            return in_array(strlen($number), $lengths);
        });

        $this->app['validator']->extend('company', function ($attribute, $value, $parameters) {
            return (bool) preg_match('/^[а-яА-ЯёЁa-zA-Z0-9_"()!,-\. ]+$/uD', $value);
        });

        $this->app['validator']->extend('text', function ($attribute, $value, $parameters) {
            return (bool) preg_match('/^[а-яА-ЯёЁa-zA-Z0-9_"()!,-\. ]+$/uD', $value);
        });
    }

    public function register()
    {
        //
    }
}