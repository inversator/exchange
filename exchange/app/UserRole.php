<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = 'roles_users';
    protected $primaryKey = 'role_id';
    protected $fillable = [
        'user_id',
        'role_id',
    ];
    public $timestamps = false;
}
