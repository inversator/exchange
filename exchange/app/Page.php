<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    public function getByUrl($url)
    {
        $res = $this->where(['url' => $url])->get()->toArray();
        if (count($res)) {
            return $res[0];
        }

        return [];
    }


}
