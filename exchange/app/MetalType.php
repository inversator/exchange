<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetalType extends Model
{
    protected $table = 'metal_types';
    protected $fillable = [
        'title', 'metal_id', 'desc'
    ];
    public $timestamps = false;

    public function metals()
    {
        return $this->belongsTo('App\Metal','metal_id');
    }

}
