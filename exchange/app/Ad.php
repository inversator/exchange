<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $table = 'ads';

    protected $fillable = [
        'user_id',
        'adType',
        'seller',
        'email',
        'phone',
        'city_id',
        'metal',
        'volume',
        'price',
        'deal',
        'text'
    ];

    public function getMetal()
    {
        return $this->hasOne('App\Metal', 'id', 'metal')->first();
    }

    public function getCity()
    {
        return $this->hasOne('App\City', 'id', 'city_id')->first();
    }

    public function getUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id')->first();
    }

    public function getPhoneAttribute($value)
    {
        return "+7 (" . substr($value, 0, 3) . ") " . substr($value, 3, 3) . "-" . substr($value, 6);
    }

    public function getDealAttribute($value)
    {
        return config('lom.dealType.' . $value);
    }
}
