<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = [
        'id', 'title', 'type', 'type_short', 'okato'
    ];
    public $timestamps = false;
}
