<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metal extends Model
{
    protected $table = 'metals';
    protected $fillable = [
       'title', 'alias', 'desc'
    ];
    public $timestamps = false;

    public function types()
    {
        return $this->hasMany('App\MetalType','metal_id','id');
    }
}
