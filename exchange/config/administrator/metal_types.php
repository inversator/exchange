<?php

return [
    'title' => 'Тип металла',
    'single' => 'тип',
    'model' => 'App\MetalType',
    'columns' => [
        'title', 'metal' => [
            'title' => 'Metal',
            'relationship' => 'metals',
            'select' => 'title',
        ],
    ],
    'filters' => [
        'metals' => [
            'title' => 'Metal',
            'type' => 'relationship',
            'name_field' => 'title',
        ]
    ],
    'edit_fields' => [
        'title' => [
            'type' => 'text',
        ],

        'desc' => [
            'type' => 'wysiwyg'
        ],
        'metals' => [
            'title' => 'Metal',
            'type' => 'relationship',
            'name_field' => 'title',
        ]
    ],
    'form_width' => '500'
];