<?php

return [
    'title' => 'Металлы',
    'single' => 'Металл',
    'model' => 'App\Metal',
    'columns' => [
        'title',
        'alias',
    ],
    'edit_fields' => [
        'title' => [
            'type' => 'text',
        ],
        'alias' => [
            'type' => 'text'
        ],
        'desc' => [
            'type' => 'wysiwyg'
        ],
    ],
    'form_width' => '500'
];