<?php
return [
    'title' => 'Pages',
    'single' => 'page',
    'model' => 'App\Page',
    'columns' => [
        'id',
        'pTitle',
        'url',
        'off'
    ],
    'edit_fields' => [
        'meta_k' => [
            'type' => 'text',
        ],
        'meta_d' => [
            'type' => 'text',
        ],
        'bTitle' => [
            'type' => 'text',
        ],
        'pTitle' => [
            'type' => 'text',
        ],
        'url' => [
            'type' => 'text'
        ],
        'off' => [
            'type' => 'bool',
        ],
        'desc' => [
            'type' => 'wysiwyg',
        ],
        'text' => [
            'type' => 'wysiwyg',
        ],
    ],
    'form_width' => '800',
];