<?php
return [
    'title' => 'Menus',
    'single' => 'item',
    'model' => 'App\Menu',
    'columns' => [
        'id',
        'active',
        'name',
        'alias',
        'position'
    ],
    'edit_fields' => [
        'active' => [
            'type' => 'bool',
        ],
        'position' => [
            'type' => 'number',
        ],
        'name' => [
            'type' => 'text'
        ],
        'alias' => [
            'type' => 'text'
        ],
        'url' => [
            'type' => 'text'
        ]
    ],
    'filters' => [
        'active' => [
            'type' => 'bool',
        ],
        'name' => [
            'type' => 'text'
        ],
    ],
];