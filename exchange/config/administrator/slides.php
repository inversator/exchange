<?php
/**
 * Created by PhpStorm.
 * User: maa
 * Date: 23.09.2016
 * Time: 11:13
 */

return [
    'title' => 'Slides',
    'single' => 'slide',
    'model' => 'App\Slider',
    'columns' => [
        'id',
        'off',
        'image' => [
            'output' => '<img src="/uploads/slides/small/(:value)" />',
        ]
    ],
    'edit_fields' => [
        'off' => [
            'type' => 'bool'
        ],
        'position' => [
            'type' => 'number',
        ],
        'image' => [
            'type' => 'image',
            'location' => public_path() . '/uploads/slides/original/',
            'sizes' => [
                [100, 32, 'auto', public_path() . '/uploads/slides/small/', 100],
                [940, 297, 'auto', public_path() . '/uploads/slides/large/', 100],
            ]
        ]
    ],

];