<?php

return [
    'title' => 'Users',
    'single' => 'user',
    'model' => 'App\User',
    'columns' => [
        'id',
        'name',
        'email',
    ],
    'edit_fields' => [
        'email' => [
            'type' => 'text',
        ],
        'name' => [
            'type' => 'text'
        ],
    ],
];